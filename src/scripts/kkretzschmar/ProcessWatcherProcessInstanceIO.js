function ProcessInstanceIO(processIcon)
{
    this.__base__ = Object;
    this.__base__();
    this.processInstance = ProcessInstance.fromIcon(processIcon);

    this.getProcessId = function() {
        return this.processInstance.processId();
    }

    this.addInputFrames = function(files) {
        var frames = this.getInputFrames();
        for (var i = 0; i < files.length; ++i) {
            var doContinue = false;
            for (var j = 0; j < frames.length; j++) {
                if (this.getInputFile(frames, j) == files[i].name && !this.isInputFileEnabled(frames, j)) {
                    doContinue = true;
                }
            }
            if (doContinue) {
                continue;
            }
            this.addNewFile(frames, files[i].name)
        }
        this.setInputFrames(frames);
    }

    this.execute = function () {
        var frames = this.getInputFrames();
        var doExecute = false;
        for (var i = 0; i < frames.length; i++) {
            if (this.isInputFileEnabled(frames, i)) {
                doExecute = true;
            }
        }
        if (doExecute) {
            this.processInstance.executeGlobal();
            return doExecute;
        }
        return doExecute;
    }

    this.disableFrames = function() {
        var frames = this.getInputFrames();
        for (var frameItemIdx = 0; frameItemIdx < frames.length; frameItemIdx++) {
            frames[frameItemIdx][0] = false;
        }
        this.setInputFrames(frames);        
    }

    // methods that should be overwritten by child-classes 
    this.setInputFrames = function(inputFilesArray) {   
    }

    this.getInputFrames = function() {
        return [[false, ""]];
    }  

    this.getInputFile = function(frames, index) {
        return frames[index][1];
    }

    this.isInputFileEnabled = function(frames, index) {
        return frames[index][0];
    }

    this.addNewFile = function(frames, file) {
        var newFrame = [true, file];
        frames.push(newFrame);
    }

    this.setOutputDirectory = function(outputDirectory) {
        this.processInstance.outputDirectory = outputDirectory;
    }

    this.getOutputDirectory = function() {
        return this.processInstance.outputDirectory;
    }

    this.createsMeasurements = function () {
        return false;
    }

    this.getMeasurements = function () {
        return []; // define array structure
    }

    this.getMeasurementDescription = function () {
        return []; // define array structure
    }

    this.sendMessage = function (instanceId, measurements) {
        if ( CoreApplication.isInstanceRunning(instanceId) )
         CoreApplication.sendMessage( instanceId, JSON.stringify(measurements));    
    }
}

ProcessInstanceIO.prototype = new Object();

// ProcessInstanceIO factory method
ProcessInstanceIO.create = function (processIcon) {
    var process = ProcessInstance.fromIcon(processIcon);
    if (process.processId() == "SubframeSelector") {
        return new SubframeProcessInstanceIO(processIcon);
    }
    if (process.processId() == "StarAlignment") {
        return new StarAlignmentProcessInstanceIO(processIcon);
    }
    if (process.processId() == "Script") {
        return new ImageSolverProcessInstanceIO(processIcon);
    }
    return undefined;
}

// SubframeSelector process subclass
function SubframeProcessInstanceIO(processIcon) {
    this.__base__ = ProcessInstanceIO;
    this.__base__( processIcon );

    this.setInputFrames = function(inputFrames) {
        this.processInstance.subframes = inputFrames;
    }

    this.getInputFrames = function() {
        return this.processInstance.subframes;
    }

    this.createsMeasurements = function () {
        return true;
    }

    this.getMeasurements = function () {
        var result = [];
        var measurements = this.processInstance.measurements;
        
        for (var i = 0; i < measurements.length ; ++i) {
            var measurement = measurements[i];
            var newMeasurement = [];
            for (var j = 0; j < measurement.length; ++j) {
                if ((j == 1  /*Enabled*/) || (j == 2  /*Locked*/)) {
                    continue;
                }
                if (j == 3 /*Path*/){
                    var tokens = measurement[j].split("/");
                    newMeasurement.push(tokens[tokens.length-1]); 
                } else {
                    newMeasurement.push(Math.round(measurement[j] * 1000000) / 1000000); 
                }
            }
            result.push(newMeasurement);
        }
        return result; 
    }

    this.getMeasurementDescription = function () {
        return [{name:'Index',               isMeasure : false},
                {name:'File',                isMeasure : false},
                {name:'Weight',              isMeasure : true},
                {name:'FWHM',                isMeasure : true},
                {name:'Eccentricity',        isMeasure : true},
                {name:'SNRWeight',           isMeasure : true},
                {name:'Median',              isMeasure : true},
                {name:'MedianMeanDev',       isMeasure : true},
                {name:'Noise',               isMeasure : true},
                {name:'NoiseRation',         isMeasure : true},
                {name:'Stars',               isMeasure : true}, 
                {name:'StarResidual',        isMeasure : true},
                {name:'FWHMMeanDev',         isMeasure : true},
                {name:'EccentricityMeanDev', isMeasure : true},
                {name:'StarResidualMeanDev', isMeasure : true} 
                ]; 
    }
}


// StarAlignent process subclass
function StarAlignmentProcessInstanceIO(processIcon) {
    this.__base__ = ProcessInstanceIO;
    this.__base__( processIcon );

    this.setInputFrames = function(inputFrames) {
        this.processInstance.targets = inputFrames;
    }

    this.getInputFrames = function() {
        return this.processInstance.targets;
    }

    this.getInputFile = function(frames, index) {
        return frames[index][2];
    }

    this.addNewFile = function(frames, file) {
        var newFrame = [true, true, file];
        frames.push(newFrame);
    }
}

